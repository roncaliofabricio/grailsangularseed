# README #

This is a seed project using Grails 2 along AngularJS, to replace the Grails views (gsp) and controllers !!

# How to use it
Fork it and be happy !  
Some points:  
- We have only one gsp file; the *index.gsp* wraps the **ng-app**  
- The **angular app** will be at */assets/javascripts/app* inside the Grails project  
- To reference the assets inside the gsp, use <asset:javascript...> or <asset:stylesheet...>

# How this was build

Just for satisfy your curiosity, these are the basic steps to build this:

1. Install node.js, npm, bower, jdk (Oracle JDK 1.8.0_31) and grails (2.4.2)
2. Create your Grails app
>grails create-app ...  
3. Create a basic angular app
>git clone --depth=1 https://github.com/angular/angular-seed.git <your-project-name>  
4. Install the Grails Assets Plugin (http://grails.org/plugin/asset-pipeline). Check if there is an older version in BuildConfig. Remove it.
5. Go to assets/javascripts and install angular:
> bower install angular --save
6. Move all .js,.css and .html from the angular app you created, to the grails /assets folder
7. Replace the content of the index.gsp by the content of the index.html from the angular app.
8. Adjust the paths along the way, for example on $routeProviders, appending '/assets..' to the path.

>The major point here, it's you can easily attach any Angular app to any Grails project following the steps above.

# References

http://grails.org/plugin/asset-pipeline  
http://stackoverflow.com/questions/21401951/how-to-integrate-angular-js-into-grails-2-3-4  
https://github.com/mannejkumar/GrailsAngularDemoApp

# Final consideration
There is a Grails plugin to do all this job: http://grails.org/plugin/angularjs-resources. Forget it. It's based on the old `resources-plugin`, which is obsolete and unflexible. Using this *"Assets approach"* we have much more flexibility and efficiency.