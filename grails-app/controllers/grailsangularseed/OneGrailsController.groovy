package grailsangularseed

class OneGrailsController {
	def oneGrailsService
    
	def getMessage() {
		println '>>>>'
		def result = oneGrailsService.serviceMethod()
		render result
	}
}
