'use strict';

angular.module('myApp.services', [])

.service('OneAngularService', function($http){
	 
	var getMessageFromOneGrailsService = function(){
		return $http.get('oneGrails/getMessage').then(function (result) {
			return result.data;
	    });	
	}
	
	return {
		getMessageFromOneGrailsService:getMessageFromOneGrailsService
	}
})