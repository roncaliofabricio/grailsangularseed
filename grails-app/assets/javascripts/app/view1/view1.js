'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'assets/app/view1/view1.html', //Yes, this is correct. It's not 'assets/javascripts..'
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', function($scope, OneAngularService) {
	$scope.msgFromGrails
	
	OneAngularService.getMessageFromOneGrailsService().then(function(result){
		//console.log(JSON.stringify(result))
		$scope.msgFromGrails = result
	})
});