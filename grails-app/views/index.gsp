<html ng-app="myApp">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My AngularJS App</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <asset:stylesheet href="lib/html5-boilerplate/dist/css/normalize.css"/>
  <asset:stylesheet href="lib/html5-boilerplate/dist/css/main.css"/>
  <asset:stylesheet href="app/app.css"/>
  <asset:javascript src="bower_components/html5-boilerplate/dist/js/vendor/modernizr-2.8.3.min.js"/>
</head>
<body>
  <ul class="menu">
    <li><a href="#!/view1">view1</a></li>
    <li><a href="#!/view2">view2</a></li>
  </ul>
 

  <div ng-view></div>

  <div>Angular seed app: v<span app-version></span></div>

  <asset:javascript src="bower_components/angular/angular.js"/>
  <asset:javascript src="bower_components/angular-route/angular-route.js"/>
  <asset:javascript src="app/app.js"/>
  <asset:javascript src="app/view1/view1.js"/>
  <asset:javascript src="app/view2/view2.js"/>
  <asset:javascript src="app/services/OneAngularService.js"/>
  <asset:javascript src="app/components/version/version.js"/>
  <asset:javascript src="app/components/version/version-directive.js"/>
  <asset:javascript src="app/components/version/interpolate-filter.js"/>
</body>
</html>
